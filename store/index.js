export const state = () => ({
  posts: []
})

export const mutations = {
  setPosts(state, value) {
    state.posts = value
  }
}
